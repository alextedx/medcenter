from flask import render_template, flash, redirect, url_for, request, g, jsonify, current_app
from flask_login import login_user, logout_user, current_user
from app.auth.forms import LoginForm
from werkzeug.urls import url_parse
from app.models import User, Promotion, Doctor, License, Partner, Service

from app.main import bp


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Неверный логин либо пароль.')
            # return redirect(url_for('main.index'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        # if not next_page or url_parse(next_page).netloc != '':
        #     next_page = url_for('main.index')
        # return redirect(next_page)
    promotions = Promotion.query.all()
    return render_template('index.html', title='Главная Медцентр', promotions=promotions, form=form)


@bp.route('/staff', methods=['GET', 'POST'])
def staff():
    doctors = Doctor.query.all()
    return render_template('staff.html', title='Наши специалисты', doctors=doctors)


@bp.route('/license', methods=['GET', 'POST'])
def license():
    scans = License.query.all()
    return render_template('license.html', title='Гарантии и лицензии', scans=scans)


@bp.route('/partners', methods=['GET', 'POST'])
def partners():
    partners = Partner.query.all()
    return render_template('partners.html', title='Партнеры', partners=partners)


@bp.route('/services', methods=['GET', 'POST'])
def services():
    services = Service.query.all()
    return render_template('services.html', title='Услуги', services=services)


@bp.route('/stock', methods=['GET', 'POST'])
def stock():
    promotions = Promotion.query.all()
    return render_template('stock.html', title='Акции', promotions=promotions)


@bp.route('/about', methods=['GET', 'POST'])
def about():
    return render_template('about.html', title='О клинике')


@bp.route('/confidential', methods=['GET', 'POST'])
def confidential():
    return render_template('confidential.html', title='Политика конфиденциальности')


@bp.route('/control', methods=['GET', 'POST'])
def control():
    return render_template('control.html', title='Адреса и телефоны контролирующих организаций')


@bp.route('/contacts', methods=['GET', 'POST'])
def contacts():
    return render_template('contacts.html', title='Контакты')

