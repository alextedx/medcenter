# import base64
# from datetime import datetime, timedelta, date
# from hashlib import md5
# import json
# import os
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from app import db, login
# from sqlalchemy.dialects.sqlite import TIME
# from sqlalchemy import event


# class PaginatedAPIMixin(object):
#     @staticmethod
#     def to_collection_dict(query, page, per_page, endpoint, **kwargs):
#         resources = query.paginate(page, per_page, False)
#         data = {
#             'items': [item.to_dict() for item in resources.items],
#             '_meta': {
#                 'page': page,
#                 'per_page': per_page,
#                 'total_pages': resources.pages,
#                 'total_items': resources.total
#             },
#             '_links': {
#                 'self': url_for(endpoint, page=page, per_page=per_page, **kwargs),
#                 'next': url_for(endpoint, page=page + 1, per_page=per_page, **kwargs) if resources.has_next else None,
#                 'prev': url_for(endpoint, page=page - 1, per_page=per_page, **kwargs) if resources.has_prev else None
#             }
#         }
#         return data


# Пользователи сайта
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(32), nullable=False)
    patronymic = db.Column(db.String(32))
    lastname = db.Column(db.String(32), nullable=False)
    phone = db.Column(db.String(32), index=True, unique=True, nullable=False)
    email = db.Column(db.String(64), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean, default=False)
    is_staff = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f'<User {self.username}>'

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    # @staticmethod
    # def verify_reset_password_token(token):
    #     try:
    #         id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])['reset_password']
    #     except:
    #         return
    #     return Users.query.get(id)
    #
    # def to_dict(self, include_email=False):
    #     data = {
    #         'id': self.id,
    #         'username': self.username,
    #         'last_seen': self.last_seen.isoformat() + 'Z',
    #         'about_me': self.about_me,
    #         'post_count': self.posts.count(),
    #         'follower_count': self.followers.count(),
    #         'followed_count': self.followed.count(),
    #         '_links': {
    #             'self': url_for('api.get_user', id=self.id),
    #             'followers': url_for('api.get_followers', id=self.id),
    #             'followed': url_for('api.get_followed', id=self.id),
    #             'avatar': self.avatar(128)
    #         }
    #     }
    #     if include_email:
    #         data['email'] = self.email
    #     return data
    #
    # def from_dict(self, data, new_user=False):
    #     for field in ['username', 'email', 'about_me']:
    #         if field in data:
    #             setattr(self, field, data[field])
    #     if new_user and 'password' in data:
    #         self.set_password(data['password'])


# Доктора
class Doctor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.String(256))
    name = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(512))
    experience = db.Column(db.String(64))
    services = db.relationship('Service', backref='doctor', lazy='dynamic')

    def __repr__(self):
        return f'{self.name} - {self.description}'


# Обследования и услуги
class Service(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(175), unique=True, nullable=False)
    description = db.Column(db.String(512))
    price = db.Column(db.Integer, nullable=False)
    doctor_id = db.Column(db.Integer, db.ForeignKey('doctor.id'))

    def __repr__(self):
        return self.name


# MtM таблица сервисов участвующих в акциях
services = db.Table('services',
                    db.Column('service_id', db.Integer, db.ForeignKey('service.id')),
                    db.Column('promotion_id', db.Integer, db.ForeignKey('promotion.id')),
                    )


# Акции на услуги
class Promotion(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(512))
    image = db.Column(db.String(256), nullable=False)      # убрать уник констаинт, добавить nullable=False
    active = db.Column(db.Boolean, default=False)
    services = db.relationship('Service', secondary=services, lazy='dynamic',
                               backref=db.backref('promotions', lazy='dynamic'))
    price = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f'{self.name}, {self.description}'


# Сканы лицензии на мед. деятельность
class License(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    scan = db.Column(db.String(256), nullable=False)     # убрать уник констаинт, добавить nullable=False


# Партнеры и сратники
class Partner(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.String(256), nullable=False)     # убрать уник констаинт, добавить nullable=False
    url = db.Column(db.String(256), unique=True)
