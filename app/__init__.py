import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os
from flask import Flask, request, current_app, url_for, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_mail import Mail
# from flask_moment import Moment
from config import Config
from flask_admin import Admin, form
from flask_admin.contrib.sqla import ModelView
from jinja2 import Markup
from werkzeug.wsgi import SharedDataMiddleware


db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_view = 'auth.login'
login.login_message = 'Please log in to access this page.'
mail = Mail()

# moment = Moment()

admin = Admin()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    mail.init_app(app)

    # moment.init_app(app)

    admin.init_app(app)

    app.add_url_rule('/media/<filename>', 'uploaded_file', build_only=True)
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {'/media': app.config['UPLOAD_FOLDER']})

    class DoctorView(ModelView):
        def _list_thumbnail(view, context, model, name):
            if not model.image:
                return ''
            # return Markup('<img src="%s">' % url_for('static', filename=form.thumbgen_filename(model.image)))
            return Markup('<img src="%s">' % url_for('uploaded_file', filename=form.thumbgen_filename(model.image)))

        column_formatters = {
            'image': _list_thumbnail
        }

        # Alternative way to contribute field is to override it completely.
        # In this case, Flask-Admin won't attempt to merge various parameters for the field.
        form_extra_fields = {
            'image': form.ImageUploadField('Фото',
                                          base_path=app.config['UPLOAD_FOLDER'],
                                          thumbnail_size=(100, 100, True))
        }

    class LicenseView(ModelView):
        def _list_thumbnail(view, context, model, name):
            if not model.scan:
                return ''
            # return Markup('<img src="%s">' % url_for('static', filename=form.thumbgen_filename(model.image)))
            return Markup('<img src="%s">' % url_for('uploaded_file', filename=form.thumbgen_filename(model.scan)))

        column_formatters = {
            'scan': _list_thumbnail
        }

        # Alternative way to contribute field is to override it completely.
        # In this case, Flask-Admin won't attempt to merge various parameters for the field.
        form_extra_fields = {
            'scan': form.ImageUploadField('Скан',
                                          base_path=app.config['UPLOAD_FOLDER'],
                                          thumbnail_size=(100, 100, True))
        }

    class PromotionView(ModelView):
        def _list_thumbnail(view, context, model, name):
            if not model.image:
                return ''
            # return Markup('<img src="%s">' % url_for('static', filename=form.thumbgen_filename(model.image)))
            return Markup('<img src="%s">' % url_for('uploaded_file', filename=form.thumbgen_filename(model.image)))

        column_formatters = {
            'image': _list_thumbnail
        }

        # Alternative way to contribute field is to override it completely.
        # In this case, Flask-Admin won't attempt to merge various parameters for the field.
        form_extra_fields = {
            'image': form.ImageUploadField('Картинка',
                                           base_path=app.config['UPLOAD_FOLDER'],
                                           thumbnail_size=(100, 100, True))
        }

    class PartnerView(ModelView):
        def _list_thumbnail(view, context, model, name):
            if not model.image:
                return ''
            # return Markup('<img src="%s">' % url_for('static', filename=form.thumbgen_filename(model.image)))
            return Markup('<img src="%s">' % url_for('uploaded_file', filename=form.thumbgen_filename(model.image)))

        column_formatters = {
            'image': _list_thumbnail
        }

        # Alternative way to contribute field is to override it completely.
        # In this case, Flask-Admin won't attempt to merge various parameters for the field.
        form_extra_fields = {
            'image': form.ImageUploadField('Promotion',
                                           base_path=app.config['UPLOAD_FOLDER'],
                                           thumbnail_size=(100, 100, True))
        }

    from app.models import User, Doctor, Service, Promotion, License, Partner
    admin.add_view(ModelView(User, db.session))
    admin.add_view(DoctorView(Doctor, db.session))
    admin.add_view(ModelView(Service, db.session))
    admin.add_view(PromotionView(Promotion, db.session))
    admin.add_view(LicenseView(License, db.session))
    admin.add_view(PartnerView(Partner, db.session))

    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    if not app.debug and not app.testing:
        if app.config['MAIL_SERVER']:
            auth = None
            if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
                auth = (app.config['MAIL_USERNAME'],
                        app.config['MAIL_PASSWORD'])
            secure = None
            if app.config['MAIL_USE_TLS']:
                secure = ()
            mail_handler = SMTPHandler(
                mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                fromaddr='no-reply@' + app.config['MAIL_SERVER'],
                toaddrs=app.config['ADMINS'], subject='Medcenter Failure',
                credentials=auth, secure=secure)
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)

        if app.config['LOG_TO_STDOUT']:
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(logging.INFO)
            app.logger.addHandler(stream_handler)
        else:
            if not os.path.exists('logs'):
                os.mkdir('logs')
            file_handler = RotatingFileHandler('logs/microblog.log', maxBytes=10240, backupCount=10)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
            file_handler.setLevel(logging.INFO)
            app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('Medcenter startup')

    return app


from app import models
