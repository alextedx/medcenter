$(document).ready(function(){
  var swiper = new Swiper('.swiper .swiper-container', {
    autoplay: {
      delay: 5000,
    },
    speed: 400,
    slidesPerView: 1,
    centeredSlides: true,
    loop: true,
    observer: true,
    observeParents: true,
  });
  (function () {
    var switchButtons = $('.switch__button');
    var switchBlocks = $('.switch__block');
  
  
    $(switchButtons[0]).addClass('switch__button--active');
    $(switchBlocks[0]).show();
  
  
    switchButtons.on('click', function () {
      var index = $(this).index();
  
      $(this).addClass('switch__button--active').siblings().removeClass('switch__button--active');
      switchBlocks.siblings().hide();
      $(switchBlocks[index]).show();
    });
  })();
  var validation = $('#feedback-form').validate({
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      message: {
        required: true
      },
      agree: {
        required: true
      }
    },
    errorPlacement: function (error, element) {
    },
  
    invalidHandler: function (form, validator) {
      if (!validator.numberOfInvalids()) return;
    },
  
    highlight: function (elem) {
      $(elem).parent().removeClass('success').addClass('error');
    },
  
    unhighlight: function (elem) {
      $(elem).parent().removeClass('error').addClass('success');
    },
  
    focusInvalid: true,
  
      submitHandler: function (formInfo, event) {
          event.preventDefault();
  
          $.ajax({
              url: $(formInfo).attr('action'),
              data: $(formInfo).serialize(),
              method: 'POST',
              success: function(result){
                  if (result.success) {
                      formEl.hide();
                      sendEl.show();
                  }
              }
          });
      }
  });
  (function () {
    $('.mask-phone').inputmask({"mask": "+7 (999) 999-99-99"});
  })();
  (function () {
    var popupButtons = $('.js-popups');
    var popups = $('.popup');
    var closeBtn = $('.popup__close');
    var body = $('body');
  
    var inputs = $('input');
  
    var clearFields = function() {
      inputs.each(function(i, item) {
        $(item).val('');
      });
    };
  
    var closePopups = function() {
      popups.hide();
      body.removeClass('overflow');
      clearFields();
    };
  
    // Open
    popupButtons.on('click', function (event) {
      event.preventDefault();
  
      var popup = $(this).data('popup');
  
      popups.hide();
      $('#' + popup + '-popup').show();
      body.addClass('overflow');
    });
  
    // Close on overlay
    popups.on('click', function (event) {
      event.preventDefault();
  
      if ($(this)[0] === event.target) {
        closePopups();
      }
    });
  
    // Close
    closeBtn.on('click', function(event) {
      event.preventDefault();
  
      closePopups();
    })
  })();
});